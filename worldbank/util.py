from iso3166 import countries


def convert_ALPHA2_to_ALPHA3(alpha2):

    try:
        data = countries.get(alpha2)
        return data.alpha3
    except Exception:
        return None


def get_country_shortname(alpha2):
    try:
        data = countries.get(alpha2)
        return data.apolitical_name
    except Exception:
        return None

