$(document).ready(function(){
    get_data()
});


function updateMap(bank){

    var map = new Datamap({
        element: document.getElementById("container"),
        projection: 'mercator',
        geographyConfig: {
            highlightBorderColor: '#0d2e63',
            popupTemplate: function(geography, data) {
                return '<div class="hoverinfo">'+ '<strong>'+geography.properties.name+'</strong>' + '  '+
                'LendProjectCost:' + data.cost +' '
            },
            highlightBorderWidth: 1
        },

        fills: {
            defaultFill: "#ABDDA4",
            authorHasTraveledTo: "#fa0fa0",
            firstInterval: '',
            secondInterval: '',
            thirdInterval: '',
            fourthInterval: '',
            fifthInterval: '',
        },

        data : createDataObj(bank), // Change data in code.
    });
}

function get_data(){
    $.ajax({
        type: 'GET',
        url: '/data',
        dataType: 'json',
        async: true,
        success: function (data){
            updateMap(data);
        },
    });
}

function createDataObj(data){
    /*
        param: data - The object is returned by the server
    */
    var code = {};
    var lendprojectcost = {};
    var tmp = ' ';
    for (var key in data){
        code[data[key]['countrycode']] = { fillKey: "authorHasTraveledTo",
                                           cost: "10000$"};
    }

    return code;
}

