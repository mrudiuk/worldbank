from flask import Flask, render_template, jsonify, url_for
from flask_pymongo import PyMongo
from .util import convert_ALPHA2_to_ALPHA3, get_country_shortname
app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'worldbank'
mongo = PyMongo(app)


@app.route('/data/')
def get_data():
    banks = mongo.db.bank.find()
    data = []
    tmp = {}
    for bank in banks:

        countrycode = convert_ALPHA2_to_ALPHA3(bank['countrycode'])

        if countrycode in tmp:
            tmp[countrycode] += bank['countryname']
        else:
            tmp[countrycode] = bank['countryname']

        if countrycode:
            data.append({
                'project_name': bank['project_name'],
                'countryname': bank['countryname'],
                'lendprojectcost': bank['lendprojectcost'],
                'countrycode': countrycode,
                #'lendprojectcostall': tmp[countrycode],
            })
        else:
            continue

    return jsonify(data)


@app.route('/map/')
def world_map():
    title = 'World Map'
    return render_template('worldmap.html', title=title)


@app.route('/')
def test():
    banks = mongo.db.bank.find()
    data = []
    banks_data = []
    for bank in banks:

        countrycode = convert_ALPHA2_to_ALPHA3(bank['countrycode'])
        name = get_country_shortname(bank['countrycode'])

        if countrycode and name:
                data.append(countrycode)
                banks_data.append({
                    'countryname': name,
                    'countrycode': countrycode,
                    'lendprojectcost': bank['lendprojectcost'],
                    'project_name': bank['project_name'],
                    },)
        else:
            continue

    data = list(set(data))

    return render_template('test.html', data={'data': data, 'banks': banks_data})


