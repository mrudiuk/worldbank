from setuptools import setup

setup(
    name='worldbank',
    packages=['worldbank'],
    include_package_data=True,
    install_requires=[
        'flask',
        'Flask-PyMongo',
        'iso3166',
    ],
)
